from django.urls import path, include
from django.views.generic import TemplateView

app_name = 'mockups'

urlpatterns = [
    # path('login/', TemplateView.as_view(template_name='mockups/login.html'), name='login'),
    path('', TemplateView.as_view(template_name='mockups/home.html'), name='home'),
    path('dahsboard/', include([
        path('', TemplateView.as_view(template_name='mockups/dashboard/index.html'), name='dashboard'),
        path('stores/', TemplateView.as_view(template_name='mockups/dashboard/store_list.html'), name='store-list'),
        path('stores/44323248376/', TemplateView.as_view(template_name='mockups/dashboard/store_detail.html'), name='store-detail'),
        path('products/', TemplateView.as_view(template_name='mockups/dashboard/product_list.html'), name='product-list'),
        path('products/72948362/', TemplateView.as_view(template_name='mockups/dashboard/product_detail.html'), name='product-detail'),
        path('orders/', TemplateView.as_view(template_name='mockups/dashboard/order_list.html'), name='order-list'),
        path('orders/A64J9B32/', TemplateView.as_view(template_name='mockups/dashboard/order_detail.html'), name='order-detail'),
        path('orders/A64J9B32/item/737367463737373', TemplateView.as_view(template_name='mockups/dashboard/order_item_detail.html'), name='order-item-detail'),
    ])),
]